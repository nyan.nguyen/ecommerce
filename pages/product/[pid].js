import React, { Component } from 'react';
import PageHeader from '../../components/ProductDetails/PageHeader';
import ProductDetailsContent from '../../components/ProductDetails/ProductDetailsContent';
import ProductTab from '../../components/ProductDetails/ProductTab';
import OtherProducts from '../../components/ProductDetails/OtherProducts';
import { ProductsApi, VariationsApi } from '../../api';
import {serverSideTranslations} from "next-i18next/serverSideTranslations";

const ProductDetails = (props) => {
    return (
      <>
        <PageHeader useSuspense={false} />
        <ProductDetailsContent props={props} useSuspense={false} />
        <ProductTab props={props} useSuspense={false} />
        <OtherProducts props={props} useSuspense={false} />
      </>
    );
}

export default ProductDetails;

export async function getServerSideProps(context) {
    const product = await ProductsApi.get({
        _id: context.query.pid
    });

    const products = await ProductsApi.getAll();
    const variations = await VariationsApi.getAll();

    return {props: {
        product: product?product:{},
        products: products?products:[],
        variations: variations?variations:[],
        ...await serverSideTranslations(context.locale, ['common']),
    }}
}