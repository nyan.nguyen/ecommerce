import React, { Component } from 'react';
import NavbarTwo from '../components/Layout/NavbarTwo';
import Banner from '../components/HomeTwo/Banner';
import WeOfferSlider from '../components/Common/WeOfferSlider';
import AboutUs from '../components/HomeTwo/AboutUs';
import VideoArea from '../components/Common/VideoArea';
import Partner from '../components/Common/Partner';
import Footer from '../components/Layout/Footer';
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import { ProductsApi } from '../api';

const Index = (props) => {
        return (
                <>
                    <Banner useSuspense={false} />
                    <AboutUs useSuspense={false} />
                    <WeOfferSlider props={props} useSuspense={false} />
                    <VideoArea useSuspense={false} />
                    <Partner useSuspense={false} />
                </>

        );
    
}

export async function getServerSideProps(context) {
  const {locale} = context;
  const products = await ProductsApi.getAll();
  
  return {props: {
    products: products?products:[],
    ...await serverSideTranslations(locale, ['common'])
  }}
}

export default Index;