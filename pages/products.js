import React, { Component } from 'react';
import NavbarTwo from '../components/Layout/NavbarTwo';
import PageHeader from '../components/Services/PageHeader';
import NewsCardContent from '../components/News1/NewsCardContent';
import Footer from '../components/Layout/Footer';
import { ProductsApi } from '../api';
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
const Products = (props) => {
    return (
        <>
            <PageHeader useSuspense={false} />
            <NewsCardContent props={props} useSuspense={false} />
        </>
    );
}
export async function getServerSideProps(context) {
  const products = await ProductsApi.getAll();
  
  return {props: {
      products: products?products:[],
      ...await serverSideTranslations(context.locale, ['common']),
  }}
}
export default Products;
