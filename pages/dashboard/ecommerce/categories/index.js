import React from 'react';
import { CategoriesApi, SuppliersApi } from '../../../../api';
import asyncComponent from '../../../../util/asyncComponent'
import cookie from 'cookie';

const CategoriesComponent = asyncComponent(() => import('../../../../routes/Ecommerce/Categories'));

const Categories = (props) => <CategoriesComponent props={props}/>;

export default Categories;

export async function getServerSideProps(context) {
    const cookies =  context.req.headers.cookie?cookie.parse(context.req.headers.cookie):"";
    const categories = await CategoriesApi.getAll({
        headers: {
            "x-auth-token" :cookies.token
        }
    });
    
    return {props: {categories: categories?categories:[]}}
}