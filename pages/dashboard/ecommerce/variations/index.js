import React from 'react';
import { VariationsApi } from '../../../../api';
import asyncComponent from '../../../../util/asyncComponent'
import cookie from 'cookie'

const VariationsComponent = asyncComponent(() => import('../../../../routes/Ecommerce/Variations'));

const Variations = (props) => <VariationsComponent props={props}/>;

export default Variations;

export async function getServerSideProps(context) {
    const cookies =  context.req.headers.cookie?cookie.parse(context.req.headers.cookie):"";
    const variations = await VariationsApi.getAll({
        headers: {
            "x-auth-token" :cookies.token
        }
    });
    
    return {props: {variations: variations?variations:[]}}
}