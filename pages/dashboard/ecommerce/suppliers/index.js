import React from 'react';
import { SuppliersApi } from '../../../../api';
import asyncComponent from '../../../../util/asyncComponent'
import cookie from 'cookie';

const SuppliersComponent = asyncComponent(() => import('../../../../routes/Ecommerce/Suppliers'));

const Suppliers = (props) => <SuppliersComponent props={props}/>;

export default Suppliers;

export async function getServerSideProps(context) {
    const cookies =  context.req.headers.cookie?cookie.parse(context.req.headers.cookie):"";
    const suppliers = await SuppliersApi.getAll({
        headers: {
            "x-auth-token" :cookies.token
        }
    });
    
    return {props: {suppliers: suppliers?suppliers:[]}}
}