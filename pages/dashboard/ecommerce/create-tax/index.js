import React from 'react';
import asyncComponent from '../../../../util/asyncComponent'

const TaxForm = asyncComponent(() => import('../../../../routes/Ecommerce/Taxes/TaxForm'));

const CreateTax = () => <TaxForm/>;

export default CreateTax;