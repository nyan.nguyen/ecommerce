import React from 'react';
import asyncComponent from '../../../../util/asyncComponent'
import cookie from 'cookie';
import { SuppliersApi } from '../../../../api';

const SupplierForm = asyncComponent(() => import('../../../../routes/Ecommerce/Suppliers/SupplierForm'));

const EditSupplier = (props) => <SupplierForm props={props}/>;

export default EditSupplier;

export async function getServerSideProps(context) {
    const cookies =  context.req.headers.cookie?cookie.parse(context.req.headers.cookie):"";
    const supplier = await SuppliersApi.get({
        _id: context.query.sid
    },{
        headers: {
            "x-auth-token" :cookies.token
        }
    });
    
    return {props: {supplier: supplier?supplier:null}}
}