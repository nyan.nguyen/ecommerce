import React from 'react';
import { EmployeesApi, SuppliersApi } from '../../../../api';
import asyncComponent from '../../../../util/asyncComponent'
import cookie from 'cookie';

const EmployeesComponent = asyncComponent(() => import('../../../../routes/Accounts/Employees'));

const Employees = (props) => <EmployeesComponent props={props}/>;

export default Employees;

export async function getServerSideProps(context) {
    const cookies =  context.req.headers.cookie?cookie.parse(context.req.headers.cookie):"";    
    const employees = await EmployeesApi.getAll({
        headers: {
            "x-auth-token" :cookies.token
        }
    });
    
    return {props: {employees: employees?employees:[]}}
}