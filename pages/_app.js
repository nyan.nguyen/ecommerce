import '../public/css/bootstrap.min.css';
import '../public/css/fontawesome.min.css';
import '../public/css/animate.min.css';
import '../public/css/flaticon.css';
import '../node_modules/react-modal-video/css/modal-video.min.css';
import 'react-image-lightbox/style.css';
import '../public/css/style.css';
import '../public/css/responsive.css';
import "../public/vendors/flag/sprite-flags-24x24.css";
import "../public/vendors/gaxon/styles.css";
import "../public/loader.css";
import "../public/vendors/noir-pro/styles.css";
import App from 'next/app';
import Head from 'next/head';
import React from 'react';
import withRedux from 'next-redux-wrapper';
import 'antd/dist/antd.css';
import "../public/vendors/style";
import "../styles/style.css"
import Loader from '../components/Shared/Loader';
import GoTop from '../components/Shared/GoTop';
import { appWithTranslation } from 'next-i18next'
import initStore from '../redux/store';
import {Provider} from "react-redux";
import LocaleProvider from "../app/core/LocaleProvider";
import {AuthProvider} from "../util/use-auth";
import Layout from "../app/core/Layout";
import { useRouter } from 'next/router';

const Page = ({Component, pageProps, store}) => {
  const router = useRouter();
  
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <title>ATR Company</title>
      </Head>
      <Provider store={store}>
        <LocaleProvider>
            <AuthProvider>
              <Layout>
                <Component {...pageProps} />
              </Layout>
            </AuthProvider>
          
        </LocaleProvider>
      </Provider>
    </>
  );
};

export default withRedux(initStore)(appWithTranslation(Page));

{/*  */}