import * as BrandsApi from './brands';
import * as SuppliersApi from './suppliers';
import * as VariationsApi from './variations';
import * as TaxesApi from './taxes';
import * as CategoriesApi from './categories';
import * as ProductsApi from './products';
import * as EmployeesApi from './employees';

export { 
    BrandsApi, 
    SuppliersApi, 
    VariationsApi, 
    TaxesApi,
    CategoriesApi,
    ProductsApi,
    EmployeesApi
}