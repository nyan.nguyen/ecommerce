import React from 'react';
import Widget from "../../../app/components/Widget";
import IntlMessages from '../../../util/IntlMessages';
import { Button, Col, Form, Input, notification, Row } from 'antd';
import { useCookies } from 'react-cookie';
import { SuppliersApi } from '../../../api';
import { useRouter } from 'next/router';
import { useIntl } from 'react-intl';
import { DASHBOARD_PATH } from '../../../config';

const SupplierForm = ({props}) => {
    const supplier = props?.supplier;
    const intl = useIntl();
    const [cookie] = useCookies();
    const router = useRouter();
    
    const onSubmitForm = (values) => {
        if(!supplier) {
            SuppliersApi.create(values, {
                headers: {
                    "x-auth-token" :cookie.token
                }
            })
            .then(response => {
                if(response.errors) {
                    console.log(response.errors);
                    notification['error']({
                        message: intl.formatMessage({id: "error.occured"})
                    })
                } else {
                    notification['success']({
                        message: intl.formatMessage({id: "supplier.create.success"})
                    });
                    router.push(`/${DASHBOARD_PATH}ecommerce/suppliers`)
                }
            })
        } else {
            SuppliersApi.update({
                ...supplier,
                ...values
            },{
                headers: {
                    "x-auth-token" :cookie.token
                }
            })
            .then(response => {
                if(response.errors) {
                    console.log(response.errors);
                    notification['error']({
                        message: intl.formatMessage({id: "error.occured"})
                    })
                } else {
                    notification['success']({
                        message: intl.formatMessage({id: "supplier.update.success"})
                    });
                    router.push(`/${DASHBOARD_PATH}ecommerce/suppliers`)
                }
            })
        }
    }

	return (
		<Widget styleName="gx-card-profile">
            <div className="ant-card-head">
                <span className="ant-card-head-title gx-mb-2">
                    {supplier?
                        <IntlMessages id="suppliers.form.edit_title"/>
                    :
                        <IntlMessages id="suppliers.form.create_title"/>
                    }
                </span>
            </div>
            <Form 
                layout="vertical" 
                hideRequiredMark
                onFinish={onSubmitForm}
                initialValues={{
                    ...supplier
                }}
            >
				<Row gutter={16}>
					<Col span={24}>
						<Form.Item
							name="name"
							label={intl.formatMessage({ id: "form.name" })}
							rules={[{
								required: true,
								message: intl.formatMessage({ id: "validation.field.required" })
							}]}
						>
							<Input/>
						</Form.Item>
					</Col>
				</Row>
				<Row gutter={16}>
					<Col span={24}>
						<Form.Item
							name="description"
							label={intl.formatMessage({ id: "form.description" })}
						>
							<Input.TextArea rows={4}/>
						</Form.Item>
					</Col>
				</Row>
				<Row gutter={16}>
					<Col span={24}>
						<Form.Item
							name="email"
							label={intl.formatMessage({ id: "form.email" })}
							rules={[
								{
									required: true,
									type: "email",
									message: intl.formatMessage({ id: "validation.field.required" })
								}
							]}
						>
							<Input rows={4}/>
						</Form.Item>
					</Col>
				</Row>
				<Row gutter={16}>
					<Col span={24}>
						<Form.Item
							name="home_phone"
							label={intl.formatMessage({ id: "form.home_phone" })}
							rules={[{
								pattern: /[0-9]{10}/,
								message: intl.formatMessage({ id: "validation.field.required" })
							}]}
						>
							<Input rows={4}/>
						</Form.Item>
					</Col>
				</Row>
				<Row gutter={16}>
					<Col span={24}>
						<Form.Item
							name="mobile_phone"
							label={intl.formatMessage({ id: "form.mobile_phone" })}
							rules={[{
								required: true,
								pattern: /[0-9]{10}/,
								message: "Please enter 10 digit of phone number"
							}]}
						>
							<Input rows={4}/>
						</Form.Item>
					</Col>
				</Row>
				<Row gutter={16}>
					<Col span={24}>
						<Form.Item
							name="address"
							label={intl.formatMessage({ id: "form.address" })}
							rules={[{
								required: true,
								message: intl.formatMessage({ id: "validation.field.required" })
							}]}
						>
							<Input rows={4}/>
						</Form.Item>
					</Col>
				</Row>
				<Row gutter={16}>
					<Col span={24}>
						<Form.Item
							name="addressDetails"
							label={intl.formatMessage({ id: "form.address_details" })}
						>
							<Input rows={4}/>
						</Form.Item>
					</Col>
				</Row>
				<Row gutter={16}>
					<Col span={24}>
						<Form.Item
							name="zip_code"
							label={intl.formatMessage({ id: "form.zip_code" })}
							rules={[{
								required: true,
								message: intl.formatMessage({ id: "validation.field.required" })
							}]}
						>
							<Input rows={4}/>
						</Form.Item>
					</Col>
				</Row>
				<Row gutter={16}>
					<Col span={24}>
						<Form.Item
							name="city"
							label={intl.formatMessage({ id: "form.city" })}
							rules={[{
								required: true,
								message: intl.formatMessage({ id: "validation.field.required" })
							}]}
						>
							<Input rows={4}/>
						</Form.Item>
					</Col>
				</Row>
				<Row gutter={16}>
					<Col span={24}>
						<Form.Item
							name="country"
							label={intl.formatMessage({ id: "form.country" })}
							rules={[{
								required: true,
								message: intl.formatMessage({ id: "validation.field.required" })
							}]}
						>
							<Input rows={4}/>
						</Form.Item>
					</Col>
				</Row>
                <Row gutter={16}>
					<Col span={24}>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" style={{float: "right"}}>
                                <IntlMessages id="button.submit"/> 
                            </Button>
                        </Form.Item>
					</Col>
				</Row>
			</Form>
		</Widget>
	);
};

export default SupplierForm;
